#!/bin/bash
# List git commits in single line format and pipe to log file
echo "Piping commits to log file"
cd /
apt-get update
apt-get install git -y
git config --global user.email "saswath.v@avasoft.com"
git config --global user.name "saswath"
git clone git@bitbucket.org:saswath1/test_2.git
cd test_2/test/log/
git checkout autocommit
git log master --graph --oneline --decorate > commit.log
git add commit.log -f
git commit -m "writing logs"
git push --force
cd /
rm -rf /test_2
